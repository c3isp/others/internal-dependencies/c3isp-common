package it.cnr.iit.common.analytic;

import java.util.HashMap;
import java.util.concurrent.Future;

public class FutureMap<T> {

	private HashMap<String, Future<T>> futureMap = new HashMap<>();

	public HashMap<String, Future<T>> getFutureMap() {
		return futureMap;
	}

	public void setFutureMap(HashMap<String, Future<T>> futureMap) {
		this.futureMap = futureMap;
	}

	public void putFuture(String sessionId, Future<T> future) {
		futureMap.put(sessionId, future);
	}

	public Future<T> getFuture(String sessionId) {
		return futureMap.get(sessionId);
	}

	public void removeFuture(String sessionId) {
		futureMap.remove(sessionId);
	}

}
