package it.cnr.iit.common.attributes;

import it.cnr.iit.xacml.attribute.AdditionalAttribute;
import it.cnr.iit.xacml.attribute.CATEGORY;

public final class AttributesUtility {

	private AttributesUtility() {
		// TODO Auto-generated constructor stub
	}

	static public AdditionalAttribute[] parseTime(String raw_start, String raw_end) {
		String[] parsedTime = new String[4];
		// startDate
		parsedTime[0] = raw_start.split("T")[0];
		// startTime
		parsedTime[1] = raw_start.split("T")[1].split("\\.")[0].replace("Z", "");
		// endDate
		parsedTime[2] = raw_end.split("T")[0];
		// endTime
		parsedTime[3] = raw_end.split("T")[1].split("\\.")[0].replace("Z", "");
		;

		AdditionalAttribute[] dateAttrs = new AdditionalAttribute[4];

		// startDate
		dateAttrs[0] = new AdditionalAttribute(CATEGORY.RESOURCE, AttributeIds.START_DATE, parsedTime[0], "date");
		// startTime
		dateAttrs[1] = new AdditionalAttribute(CATEGORY.RESOURCE, AttributeIds.START_TIME, parsedTime[1], "time");
		// endDate
		dateAttrs[2] = new AdditionalAttribute(CATEGORY.RESOURCE, AttributeIds.END_DATE, parsedTime[2], "date");
		// endTime
		dateAttrs[3] = new AdditionalAttribute(CATEGORY.RESOURCE, AttributeIds.END_TIME, parsedTime[3], "time");

		return dateAttrs;
	}

	static public String unparseTime(String date, String time) {
		return date + "T" + time + ".0Z";
	}

}
