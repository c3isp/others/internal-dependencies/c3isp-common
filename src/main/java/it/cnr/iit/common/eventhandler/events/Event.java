package it.cnr.iit.common.eventhandler.events;

import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import it.cnr.iit.common.reject.Reject;

public class Event {

	private HashMap<String, String> additionalProperties;

	private static final Logger LOGGER = Logger.getLogger(Event.class.getName());

	public Event() {
		additionalProperties = new HashMap<>();
	}

	public Map<String, String> getAdditionalProperties() {
		return additionalProperties;
	}

	public void setAdditionalProperties(Map<String, String> additionalProperties) {
		Reject.ifNull(additionalProperties, "null additional properties");
		this.additionalProperties = (HashMap<String, String>) additionalProperties;
	}

	@JsonIgnore
	public String getProperty(String key) {
		return getAdditionalProperties().get(key);
	}

	@JsonIgnore
	public void setProperty(String key, String value) {
		getAdditionalProperties().put(key, value);
	}

	@JsonIgnore
	public boolean hasProperty(String key) {
		return getAdditionalProperties().containsKey(key);
	}

	@Override
	public String toString() {
		try {
			return new ObjectMapper().writeValueAsString(this);
		} catch (JsonProcessingException e) {
			return "";
		}
	}
}
