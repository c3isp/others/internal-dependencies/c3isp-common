package it.cnr.iit.common.eventhandler.events.bundlemanager;

import java.util.UUID;

import it.cnr.iit.common.eventhandler.events.RequestEvent;

public abstract class BundleManagerBaseEvent extends RequestEvent {

	public BundleManagerBaseEvent(String eventType, String requestID) {
		setEventType(eventType);
		setRequestID(requestID);
	}

	public BundleManagerBaseEvent(String eventType) {
		setEventType(eventType);
		setRequestID(UUID.randomUUID().toString());
	}

//	@JsonIgnore
//	public String getRequestID() {
//		return getAdditionalProperties().get(BundleManagerEventConstants.REQUEST_ID);
//	}
//
//	@JsonIgnore
//	public void setRequestID(String requestID) {
//		getAdditionalProperties().put(BundleManagerEventConstants.REQUEST_ID, requestID);
//	}
}
