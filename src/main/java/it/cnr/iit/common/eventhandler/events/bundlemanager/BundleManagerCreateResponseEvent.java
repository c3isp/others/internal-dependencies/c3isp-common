package it.cnr.iit.common.eventhandler.events.bundlemanager;

public class BundleManagerCreateResponseEvent extends BundleManagerEvent {

	public BundleManagerCreateResponseEvent(String dposId, String requestID) {
		super(BundleManagerEventTypes.CREATE_RESPONSE, dposId, requestID);
	}

}
