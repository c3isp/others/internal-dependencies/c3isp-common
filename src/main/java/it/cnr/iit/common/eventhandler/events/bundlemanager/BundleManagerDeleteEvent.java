package it.cnr.iit.common.eventhandler.events.bundlemanager;

public class BundleManagerDeleteEvent extends BundleManagerEvent {

	public BundleManagerDeleteEvent(String dposId, String requestID) {
		super(BundleManagerEventTypes.DELETE, dposId, requestID);
	}

	public BundleManagerDeleteEvent(String dposId) {
		super(BundleManagerEventTypes.DELETE, dposId);
	}

	public String getSessionId() {
		return getRequestID();
	}

	public void setSessionId(String sessionId) {
		setRequestID(sessionId);
	}

}
