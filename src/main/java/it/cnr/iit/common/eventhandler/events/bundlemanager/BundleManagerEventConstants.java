package it.cnr.iit.common.eventhandler.events.bundlemanager;

public class BundleManagerEventConstants {

	public static final String REQUEST_ID = "requestID";
	public static final String DSA_ID = "dsaId";
	public static final String METADATA_FILE = "metadataFile";
	public static final String CTI_FILE = "ctiFile";
	public static final String DPOS_ID = "dposId";
	public static final String RESULT = "result";
	public static final String FILE_CONTENT = "fileContent";
	public static final String FILE_NAME = "fileName";
	public static final String PAYLOAD_FORMAT = "payloadFormat";
	public static final String PAYLOAD_FORMAT_CLEAR = "ClearFormat";
	public static final String TMP_SYMMETRIC_KEY = "tmpSymmetricKey";

	public static final String CONTENT_URI = "contentUri";
	public static final String METADATA_URI = "metadataUri";
	public static final String POLICY_URI = "policyUri";

	private BundleManagerEventConstants() {
	}

}
