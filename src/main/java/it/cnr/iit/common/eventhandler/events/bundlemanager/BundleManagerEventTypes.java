package it.cnr.iit.common.eventhandler.events.bundlemanager;

public class BundleManagerEventTypes {

	public static final String CREATE = "bmc";
	public static final String CREATE_RESPONSE = "bmcr";
	public static final String READ = "bmr";
	public static final String READ_RESPONSE = "bmrr";
	public static final String DELETE = "bmd";
	public static final String DELETE_RESPONSE = "bmdr";

	private BundleManagerEventTypes() {
	}

}
