package it.cnr.iit.common.eventhandler.events.bundlemanager;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class BundleManagerReadEvent extends BundleManagerEvent {

	public BundleManagerReadEvent(String dposId, String requestID, String sessionID) {
		super(BundleManagerEventTypes.READ, dposId, requestID);
		setsessionID(sessionID);
		setPayloadFormat(BundleManagerEventConstants.PAYLOAD_FORMAT_CLEAR);
	}

	public BundleManagerReadEvent(String dposId, String sessionID) {
		super(BundleManagerEventTypes.READ, dposId);
		setsessionID(sessionID);
		setPayloadFormat(BundleManagerEventConstants.PAYLOAD_FORMAT_CLEAR);
	}

	public BundleManagerReadEvent(String dposId, String payloadFormat, String requestID, String sessionID) {
		super(BundleManagerEventTypes.READ, dposId, requestID);
		setsessionID(sessionID);
		setPayloadFormat(payloadFormat);
	}

	@JsonIgnore
	public String getPayloadFormat() {
		return getAdditionalProperties().get(BundleManagerEventConstants.PAYLOAD_FORMAT);
	}

	@JsonIgnore
	public void setPayloadFormat(String payloadFormat) {
		getAdditionalProperties().put(BundleManagerEventConstants.PAYLOAD_FORMAT, payloadFormat);
	}

	public String getsessionID() {
		return getRequestID();
	}

	public void setsessionID(String sessionID) {
		setRequestID(sessionID);
	}
}
