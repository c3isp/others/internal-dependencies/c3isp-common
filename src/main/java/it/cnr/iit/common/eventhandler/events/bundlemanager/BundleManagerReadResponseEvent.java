package it.cnr.iit.common.eventhandler.events.bundlemanager;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class BundleManagerReadResponseEvent extends BundleManagerBaseEvent {

	public BundleManagerReadResponseEvent(String contentUri, String policyUri, String metadataUri, String payloadFormat,
			String requestID) {
		super(BundleManagerEventTypes.READ_RESPONSE, requestID);
		setContentUri(contentUri);
		setPolicyUri(policyUri);
		setMetadataUri(metadataUri);
		setPayloadFormat(payloadFormat);
	}

	@JsonIgnore
	public String getPayloadFormat() {
		return getAdditionalProperties().get(BundleManagerEventConstants.PAYLOAD_FORMAT);
	}

	@JsonIgnore
	public void setPayloadFormat(String payloadFormat) {
		getAdditionalProperties().put(BundleManagerEventConstants.PAYLOAD_FORMAT, payloadFormat);
	}

	@JsonIgnore
	public String getContentUri() {
		return getAdditionalProperties().get(BundleManagerEventConstants.CONTENT_URI);
	}

	@JsonIgnore
	public void setContentUri(String contentUri) {
		getAdditionalProperties().put(BundleManagerEventConstants.CONTENT_URI, contentUri);
	}

	@JsonIgnore
	public String getPolicyUri() {
		return getAdditionalProperties().get(BundleManagerEventConstants.POLICY_URI);
	}

	@JsonIgnore
	public void setPolicyUri(String policyUri) {
		getAdditionalProperties().put(BundleManagerEventConstants.POLICY_URI, policyUri);
	}

	@JsonIgnore
	public String getMetadataUri() {
		return getAdditionalProperties().get(BundleManagerEventConstants.METADATA_URI);
	}

	@JsonIgnore
	public void setMetadataUri(String metadataUri) {
		getAdditionalProperties().put(BundleManagerEventConstants.METADATA_URI, metadataUri);
	}

}
