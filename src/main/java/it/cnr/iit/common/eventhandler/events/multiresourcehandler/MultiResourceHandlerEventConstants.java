package it.cnr.iit.common.eventhandler.events.multiresourcehandler;

public class MultiResourceHandlerEventConstants {

	public static final String REQUEST_ID = "requestID";
	public static final String XACML_POLICY = "xacmlPolicy";
	public static final String XACML_REQUEST = "xacmlRequest";
	public static final String XACML_POLICIES = "xacmlPolicies";
	public static final String XACML_REQUESTS = "xacmlRequests";

	public static final String SESSION_ID_DEFAULT_VALUE = "000";

	private MultiResourceHandlerEventConstants() {
	}

}
