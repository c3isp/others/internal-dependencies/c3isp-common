package it.cnr.iit.common.eventhandler.events.multiresourcehandler;

import java.util.Map;

public class MultiResourceHandlerEventEndAccess extends MultiResourceHandlerEvent {

	public MultiResourceHandlerEventEndAccess(String sessionId) {
		super(MultiResourceHandlerEventTypes.END_ACCESS);
		Map<String, String> additionalProperties = getAdditionalProperties();

		this.setSessionID(sessionId);
	}
}
