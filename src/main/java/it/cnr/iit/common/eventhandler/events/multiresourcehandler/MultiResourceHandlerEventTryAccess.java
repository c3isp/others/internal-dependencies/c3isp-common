package it.cnr.iit.common.eventhandler.events.multiresourcehandler;

import java.util.Map;
import java.util.UUID;

public class MultiResourceHandlerEventTryAccess extends MultiResourceHandlerEvent {

	public MultiResourceHandlerEventTryAccess(String xacmlPolicy, String xacmlRequest) {
		super(MultiResourceHandlerEventTypes.TRY_ACCESS);
		Map<String, String> additionalProperties = getAdditionalProperties();

		additionalProperties.put(MultiResourceHandlerEventConstants.XACML_POLICY, xacmlPolicy);
		additionalProperties.put(MultiResourceHandlerEventConstants.XACML_REQUEST, xacmlRequest);
//		JsonUtility.getJsonStringFromObject(xacmlPolicy, false)
//				.ifPresent(p -> setProperty(MultiResourceHandlerEventConstants.XACML_POLICYIES, p));
//		JsonUtility.getJsonStringFromObject(xacmlRequest, false)
//				.ifPresent(r -> setProperty(MultiResourceHandlerEventConstants.XACML_REQUESTS, r));
		setRequestID(UUID.randomUUID().toString());
		setSessionID(MultiResourceHandlerEventConstants.SESSION_ID_DEFAULT_VALUE);

	}

}
