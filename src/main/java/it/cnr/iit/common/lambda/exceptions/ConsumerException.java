package it.cnr.iit.common.lambda.exceptions;

import java.util.function.Consumer;

public interface ConsumerException<T, E extends Throwable> {
	void accept(T t) throws E;

	static <T, E extends Throwable> Consumer<T> unchecked(ConsumerException<T, E> consumer) {
		return (t) -> {
			try {
				consumer.accept(t);
			} catch (Throwable e) {
				throw new RuntimeException(e);
			}
		};
	}
}
