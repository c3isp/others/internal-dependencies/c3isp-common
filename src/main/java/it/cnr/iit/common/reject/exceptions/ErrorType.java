package it.cnr.iit.common.reject.exceptions;

public enum ErrorType {
	SYSTEM_ERROR, DATA_ERROR, DAO_ERROR, CONFIGURATION_ERROR, PERMANENT_ERROR, TRANSIENT_ERROR, USAGE_ERROR,
}
