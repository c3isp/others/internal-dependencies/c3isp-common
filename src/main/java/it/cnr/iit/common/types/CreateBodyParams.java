package it.cnr.iit.common.types;

import com.fasterxml.jackson.annotation.JsonProperty;

public class CreateBodyParams {

	@JsonProperty(value = "input_metadata")
	private String metadata;

	@JsonProperty(value = "cipheredFilePath")
	private String cipheredFilePath;

	@JsonProperty(value = "tmpSymmetricalKey")
	private String tmpSymmetricalKey;

	public CreateBodyParams() {

	}

	public CreateBodyParams(String metadata, String filePath, String tmpKey) {
		this.metadata = metadata;
		this.cipheredFilePath = filePath;
		this.tmpSymmetricalKey = tmpKey;
	}

	@JsonProperty(value = "input_metadata")
	public String getMetadata() {
		return metadata;
	}

	@JsonProperty(value = "input_metadata")
	public void setMetadata(String metadata) {
		this.metadata = metadata;
	}

	@JsonProperty(value = "cipheredFilePath")
	public String getCipheredFilePath() {
		return cipheredFilePath;
	}

	@JsonProperty(value = "cipheredFilePath")
	public void setCipheredFilePath(String cipheredFilePath) {
		this.cipheredFilePath = cipheredFilePath;
	}

	@JsonProperty(value = "tmpSymmetricalKey")
	public String getTmpSymmetricalKey() {
		return tmpSymmetricalKey;
	}

	@JsonProperty(value = "tmpSymmetricalKey")
	public void setTmpSymmetricalKey(String tmpSymmetricalKey) {
		this.tmpSymmetricalKey = tmpSymmetricalKey;
	}

}
