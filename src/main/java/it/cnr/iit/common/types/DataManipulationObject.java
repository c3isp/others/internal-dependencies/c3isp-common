package it.cnr.iit.common.types;

import java.util.HashMap;
import java.util.Map;

public class DataManipulationObject {
	private String name;
	private String param;
	private String options;
	private Map<String, String> additionalInfo;

	public DataManipulationObject(String name, String param, String options) {
		this.name = name;
		this.param = param;
		this.options = options;
		this.additionalInfo = new HashMap<String, String>();
	}

	public DataManipulationObject(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getParam() {
		return param;
	}

	public void setParam(String param) {
		this.param = param;
	}

	public String getOptions() {
		return options;
	}

	public void setOptions(String options) {
		this.options = options;
	}

	public Map<String, String> getAdditionalInfo() {
		return additionalInfo;
	}

	public void setAdditionalInfo(Map<String, String> additionalInfo) {
		this.additionalInfo = additionalInfo;
	}

	public void addAdditionalInfo(String name, String value) {
		this.additionalInfo.put(name, value);
	}

}
