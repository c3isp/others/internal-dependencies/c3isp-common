package it.cnr.iit.common.types;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class MailJson {

	@JsonProperty("filename")
	private String filename;

	@JsonProperty("content")
	private String content;

	public MailJson() {

	}

	public MailJson(String filename, String content) {
		this.filename = filename;
		this.content = content;
	}

	@JsonProperty("filename")
	public String getFilename() {
		return filename;
	}

	@JsonProperty("filename")
	public void setFilename(String filename) {
		this.filename = filename;
	}

	@JsonProperty("content")
	public String getContent() {
		return content;
	}

	@JsonProperty("content")
	public void setContent(String content) {
		this.content = content;
	}

}
