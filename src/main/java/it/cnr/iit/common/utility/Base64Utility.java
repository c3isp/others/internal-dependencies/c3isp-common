package it.cnr.iit.common.utility;

import java.util.Base64;

public final class Base64Utility {

	private Base64Utility() {
	}

	public static String encodeStringBase64(String data) {
		return Base64.getEncoder().encodeToString(data.getBytes());

	}

	public static String decodeBase64(String data) {
		return new String(Base64.getDecoder().decode(data));
	}

}
