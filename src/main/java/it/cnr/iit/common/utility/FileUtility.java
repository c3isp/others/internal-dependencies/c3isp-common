package it.cnr.iit.common.utility;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.util.Optional;
import java.util.logging.Logger;

import org.springframework.web.multipart.MultipartFile;

public class FileUtility {

	private static Logger log = Logger.getLogger(FileUtility.class.getName());

	private FileUtility() {
	}

	public static Optional<File> getFile(String uri) {
		try {
			return Optional.of(new File(new URI(uri)));
		} catch (URISyntaxException e) {
			//
		}
		return Optional.empty();
	}

	public static File convert(MultipartFile file) throws IOException {
		File convFile = new File(file.getOriginalFilename());
		convFile.createNewFile();
		FileOutputStream fos = new FileOutputStream(convFile);
		fos.write(file.getBytes());
		fos.close();
		return convFile;
	}

	public static String readFileAsString(File file) {
		String res = null;
		try {
			res = new String(Files.readAllBytes(file.toPath()));
		} catch (IOException e) {
			e.printStackTrace();
		}
		return res;
	}

	public static String getFileExtension(String f) {
		String ext = "";
		int i = f.lastIndexOf('.');
		if (i > 0 && i < f.length() - 1) {
			ext = f.substring(i + 1);
		}
		return ext;
	}

}
