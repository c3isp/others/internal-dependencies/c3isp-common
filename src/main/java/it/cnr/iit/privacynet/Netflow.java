package it.cnr.iit.privacynet;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Netflow {

	@JsonProperty(value = "src_ip")
	private String src_ip;

	@JsonProperty(value = "dst_ip")
	private String dst_ip;

	@JsonProperty(value = "proto")
	private int proto;

	@JsonProperty(value = "duration")
	private Number duration;

	@JsonProperty(value = "date")
	private Number date;

	public Netflow() {

	}

	@JsonProperty(value = "src_ip")
	public String getSrc_ip() {
		return src_ip;
	}

	@JsonProperty(value = "src_ip")
	public void setSrc_ip(String src_ip) {
		this.src_ip = src_ip;
	}

	@JsonProperty(value = "dst_ip")
	public String getDst_ip() {
		return dst_ip;
	}

	@JsonProperty(value = "dst_ip")
	public void setDst_ip(String dst_ip) {
		this.dst_ip = dst_ip;
	}

	@JsonProperty(value = "proto")
	public int getProto() {
		return proto;
	}

	@JsonProperty(value = "proto")
	public void setProto(int proto) {
		this.proto = proto;
	}

	@JsonProperty(value = "duration")
	public Number getDuration() {
		return duration;
	}

	@JsonProperty(value = "duration")
	public void setDuration(Number duration) {
		this.duration = duration;
	}

	@JsonProperty(value = "date")
	public Number getDate() {
		return date;
	}

	@JsonProperty(value = "date")
	public void setDate(Number date) {
		this.date = date;
	}

}
