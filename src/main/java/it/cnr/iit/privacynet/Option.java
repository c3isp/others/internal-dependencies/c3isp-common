package it.cnr.iit.privacynet;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Option {

	@JsonProperty(value = "name")
	private String name;

	@JsonProperty(value = "arguments")
	private String[] arguments;

	public Option() {

	}

	@JsonProperty(value = "name")
	public String getName() {
		return name;
	}

	@JsonProperty(value = "name")
	public void setName(String name) {
		this.name = name;
	}

	@JsonProperty(value = "arguments")
	public String[] getArguments() {
		return arguments;
	}

	@JsonProperty(value = "arguments")
	public void setArguments(String[] arguments) {
		this.arguments = arguments;
	}

}
