package it.cnr.iit.privacynet;

import com.fasterxml.jackson.annotation.JsonProperty;

public class PrivacyNetBodyParams {

	@JsonProperty(value = "NetflowRaw")
	private String data;

	@JsonProperty(value = "AnonymizationOptionArray")
	private Option[] options;

	public PrivacyNetBodyParams() {

	}

	@JsonProperty(value = "NetflowRaw")
	public String getData() {
		return data;
	}

	@JsonProperty(value = "NetflowRaw")
	public void setData(String data) {
		this.data = data;
	}

	@JsonProperty(value = "AnonymizationOptionArray")
	public Option[] getOptions() {
		return options;
	}

	@JsonProperty(value = "AnonymizationOptionArray")
	public void setOptions(Option[] options) {
		this.options = options;
	}

}
